using System.Linq;
using System.IO;
using System;
using System.Collections.Generic;
namespace DmvAppointmentScheduler
{
  class GeneticAlgorithm{
      int populationSize=20;
      int maxGeneration= 1000;//2000;
      float eliteRate = 0.05f;
      float crossOverRate = 0.9f;
      float mutationRate= 0.1f;
      CustomerList customers;
      TellerList tellers;

      public Random random = new Random();
      
      //StreamWriter sw =null;
      List<string> uniqueTypeOfTellerWithNoMatchingCustomer;
      List<Teller> tellerWithNoMatchingCustomer;
      IEnumerable<IGrouping<string, Teller>>  tellerGroupBySpecTypeQuery;
      public GeneticAlgorithm(CustomerList customers, TellerList tellers){
        this.customers= customers;
        this.tellers=tellers;

         (tellerWithNoMatchingCustomer, uniqueTypeOfTellerWithNoMatchingCustomer)= 
           CheckTellerWithoutAnyMatchingCust(customers, tellers);
          //tellerGroupBySpecType omits teller who doesnt have any matching customer type
           tellerGroupBySpecTypeQuery= tellers.Teller.Where(x=> !uniqueTypeOfTellerWithNoMatchingCustomer.Contains(x.specialtyType))
           .GroupBy(eachTeller => eachTeller.specialtyType);
      }

        public Chromosome startGA(){
            int generations =0;
            int newPopulationChrIndexAfterElite=0;
            double obtainedFitness = 0;
            string outPath = Path.Combine(Environment.CurrentDirectory, @"InputData\", "out.txt");
            //sw = new StreamWriter(outPath);
            Chromosome[] chromosomePopA=new Chromosome[populationSize];//population 1
            // Chromosome[] chromosomePopB=new Chromosome[populationSize];// population 2
            Chromosome[] chromosomeTemp = new Chromosome[populationSize];
            
            /*......................................................................................................................
            ............................. Starting Genetic Algorithm ...............................................................
            ........................................................................................................................
            */
            Console.WriteLine("starting GA ....");

            //initialize population
            initializePopulation(ref chromosomePopA); 
            
            //sw.WriteLine(String.Format("\n===================== Generation {0} ======================================\n ===========================================================================================",generations));
            chromosomePopA = sortPopulationByFitness(chromosomePopA);
            Console.WriteLine("Finished initializing population and sorting population according to fitness ......");

            while (generations < maxGeneration){
                generations += 1;
                Chromosome[] chromosomePopB=new Chromosome[populationSize];// population 2
                //doElite
                newPopulationChrIndexAfterElite= doElite(chromosomePopA, ref chromosomePopB );
                Console.WriteLine("========================= GENERATION: {0} ========================= ",generations);
                Console.WriteLine("Elite index is from 0 to {0}",newPopulationChrIndexAfterElite);

                //do CrossOVer
                Console.WriteLine("Starting Crossover ......");
                int indexAfterCrossOver=doCrossOver(chromosomePopA, ref chromosomePopB, newPopulationChrIndexAfterElite+1);
                //Console.WriteLine("Ending Crossover ########");

                //Fill the rest 
                Console.WriteLine("Starting Filling the rest ......");
                int indAfterFillRest = fillRestAfterCrossOver(ref chromosomePopB,indexAfterCrossOver+1);
                //Console.WriteLine("Ending Filling the rest ########");

                //start mutation
                Console.WriteLine("Starting Mutation after filling entire population......");
                doMutation(chromosomePopB);// after this newly created population is in chromosomePopB
                //Console.WriteLine("Ending Mutation after filling entire population ########");
 
                //do population swap
                for(int i = 0; i < populationSize; i++){
                  chromosomeTemp[i] = new Chromosome();
                  chromosomeTemp[i].appointmentList = chromosomePopA[i].appointmentList;
                  chromosomeTemp[i].fitness = chromosomePopA[i].fitness;

                  chromosomePopA[i].appointmentList = chromosomePopB[i].appointmentList;
                  chromosomePopA[i].fitness = chromosomePopB[i].fitness;
    
                  chromosomePopB[i].appointmentList = chromosomeTemp[i].appointmentList;
                  chromosomePopB[i].fitness = chromosomeTemp[i].fitness;

                }

                //calculate fitness of new population
                for(int r= 0; r<populationSize; r++){
                    chromosomePopA[r].fitness= calculateFitness(chromosomePopA[r]);
                }
              
                chromosomePopA = sortPopulationByFitness(chromosomePopA);
                obtainedFitness =chromosomePopA[0].fitness;             
               
            }//end of while loop    
            return chromosomePopA[0];
        }

        int doCrossOver(Chromosome[] chromosomePopA, ref Chromosome[] chromosomePopB, int startIndexForCrossOver){
          
          int crossOverPoint=0;
          double totalFitness=0;
          double rand1,rand2;
          int firstSelectionIndex=0;
          int secondSelectionIndex=0;
          int numberOfCrossOVer=(int) ((crossOverRate * populationSize) / 2);
          
          for (int i = 0; i < populationSize; i++) {

				  	totalFitness += chromosomePopA[i].fitness;//to execute roulette wheel
				  }
          for (int I = 0; I < numberOfCrossOVer; I++) {
              Chromosome tempChr1=null; //candidate 1 for crossOver
              Chromosome tempChr2= null; //candidate 2 for crossOver
              Chromosome tempChrChild1=new Chromosome(); //child 1 from crossOver
              Chromosome tempChrChild2= new Chromosome(); //child 2 from crossOver
              //processing for 1st candidate 
              rand1= getRandomNumberBetween(0,totalFitness );
              firstSelectionIndex=0;
              while (rand1>0)
              {
                  rand1= rand1 - chromosomePopA[firstSelectionIndex].fitness;
                  firstSelectionIndex++;
              }
             tempChr1=  chromosomePopA[firstSelectionIndex-1];

              //processing for 2nd candidate 
              rand2= getRandomNumberBetween(0,totalFitness );
              secondSelectionIndex= 0;
               while (rand2>0)
              {
                  rand2= rand2 - chromosomePopA[secondSelectionIndex].fitness;
                  secondSelectionIndex++;
              }
              tempChr2=  chromosomePopA[secondSelectionIndex-1];

              crossOverPoint = (int) Math.Ceiling((tempChr1.appointmentList.Count-1)	* getRandomNumberBetween(0, 1));
              //Console.WriteLine("Cross Over Point = {0}",crossOverPoint );
              for (int l = 0; l < crossOverPoint; l++) {
                  tempChrChild1.appointmentList.Add(tempChr1.appointmentList[l]) ;
                  tempChrChild2.appointmentList.Add(tempChr2.appointmentList[l]);
              }

              for (int m = crossOverPoint; m < tempChr1.appointmentList.Count; m++) {
                tempChrChild1.appointmentList.Add(tempChr2.appointmentList[m]);
                tempChrChild2.appointmentList.Add(tempChr1.appointmentList[m]);
              }

              chromosomePopB[startIndexForCrossOver] = new Chromosome();
              chromosomePopB[startIndexForCrossOver].appointmentList = tempChrChild1.appointmentList;
              startIndexForCrossOver++;

              chromosomePopB[startIndexForCrossOver] = new Chromosome();
              chromosomePopB[startIndexForCrossOver].appointmentList =tempChrChild2.appointmentList;
              startIndexForCrossOver++;


          }//end of for loop
          return startIndexForCrossOver-1;
        }

        int fillRestAfterCrossOver(ref Chromosome[] chromosomePopB,int startIndexForFill){
            int filleRestCount = 0;
            for (int p = startIndexForFill ; p < populationSize; p++) {
                  chromosomePopB[p] =new Chromosome();
                  chromosomePopB[p]=generateChromosome(chromosomePopB[p], false);// false to indicate that generate chromosome but do not calculate fitness
                  filleRestCount++;
            }
           // Console.WriteLine(String.Format("After filling rest, the index is at {0}",(startIndexForFill+filleRestCount)));
            return startIndexForFill+filleRestCount;
        }

        void doMutation(Chromosome[] chromosomePopB){
          int numberOfMutation = (int) (mutationRate * populationSize);
          int numElite = (int) (eliteRate * populationSize);
          int chromosomeIndexToMutate=0;
          int indexToMutate = 0;
          for (int q = 0; q < numberOfMutation; q++) {
              chromosomeIndexToMutate = (int)Math.Ceiling(numElite-1 + (populationSize-numElite)*getRandomNumberBetween(0, 1));
              indexToMutate = (int) Math.Ceiling((chromosomePopB[chromosomeIndexToMutate].appointmentList.Count-1)	* getRandomNumberBetween(0, 1));
   
              Appointment appointmentToMutate=chromosomePopB[chromosomeIndexToMutate].appointmentList[indexToMutate];
             // Console.WriteLine("====> Checking ===>Before mutation id of teller {0}",appointmentToMutate.teller.id );
               if (appointmentToMutate.customer.type == appointmentToMutate.teller.specialtyType)
              {
                  foreach(var group in tellerGroupBySpecTypeQuery){

                     if(group.Key.Equals(appointmentToMutate.customer.type)){
                          
                          Teller currentTellerToServe=group.ElementAt(random.Next(0, group.Count()));// select the serving teller(whose type matches customer's type) random;
                          appointmentToMutate.teller= currentTellerToServe;
                          break;
                     }
                  }
              }
              else
              {
                  if(uniqueTypeOfTellerWithNoMatchingCustomer!=null && tellerWithNoMatchingCustomer.Any()){
                      Teller currentTellerToServe=tellerWithNoMatchingCustomer.ElementAt(random.Next(0, tellerWithNoMatchingCustomer.Count));
                      appointmentToMutate.teller= currentTellerToServe;
                    }
              }
             appointmentToMutate.reCalculateDuration();
              chromosomePopB[chromosomeIndexToMutate].appointmentList[indexToMutate] = appointmentToMutate;

          }

        }
        void initializePopulation(ref Chromosome[] chromosomePopulation){
          Console.WriteLine("Initializing population generation ......");
          for (int i = 0; i < populationSize; i++) 
          {
            chromosomePopulation[i]= new Chromosome();
            chromosomePopulation[i] = generateChromosome(chromosomePopulation[i], true);
          }
          //Console.WriteLine("End of population generation ########");
        }

        Chromosome generateChromosome(Chromosome chromosome, Boolean initialPopulation){ 

           foreach(Customer customer in customers.Customer){
              Boolean match=false;
              foreach(var group in tellerGroupBySpecTypeQuery){
                //if customer's service type matches the tellergrpups' speciality type,
                    //get the serving teller randomly from the group of teller whose type matches this customer's type
                     if(group.Key.Equals(customer.type)){
                          match=true;
                          Teller currentTellerToServe=group.ElementAt(random.Next(0, group.Count()));// select the serving teller(whose type matches customer's type) random;
                        
                          var appointment = new Appointment(customer, currentTellerToServe);
                          chromosome.appointmentList.Add(appointment);
                          break;
                     }
              }
              //if this customer's type didnt match with any teller above, assign this customer to such teller who doesnt have any matching customer randomly
               if(!match){
                   //if customers service type didnt match with any teller, we can send them to the teller with no matching customer type
                    if(uniqueTypeOfTellerWithNoMatchingCustomer!=null && tellerWithNoMatchingCustomer.Any()){
                      Teller currentTellerToServe=tellerWithNoMatchingCustomer.ElementAt(random.Next(0, tellerWithNoMatchingCustomer.Count));
  
                      var appointment = new Appointment(customer, currentTellerToServe);
                      chromosome.appointmentList.Add(appointment);

                    }
                    // else{
                    //   console.WriteLine("There are no teller who can assist this customer.");
                    // }
               }
           }
         
          if(initialPopulation){
            //calculate fitness
            chromosome.fitness= calculateFitness(chromosome);
          }
          return chromosome;
        }

        double calculateFitness(Chromosome chromosome){
            //150 fitness score for 150 teller
            //sum of all appointment duration for each teller
            //get the teller with max appointment duration sum 
            var tellerAppointments =
                from appointment in chromosome.appointmentList
                group appointment by appointment.teller into tellerGroup
                select new
                {
                    teller = tellerGroup.Key, 
                    totalDuration = tellerGroup.Sum(x => x.duration),
                };
            return tellerAppointments.OrderBy(i => i.totalDuration).LastOrDefault().totalDuration;
           
        }

        Chromosome[] sortPopulationByFitness(Chromosome[] chromosomePopulation){
          Console.WriteLine("Sorting each population by ascending fitness ......");
            //sort population based on ascending fitness function since we want the best population configuration where the total served duration for all appointment is minimum
            Array.Sort(chromosomePopulation, delegate(Chromosome chr1, Chromosome chr2) {
                    return chr1.fitness.CompareTo(chr2.fitness);
                  } );
            
           //Console.WriteLine("Ending Sorting each population by ascending fitness ......"); 
           return chromosomePopulation;
        }

        int doElite(Chromosome[] chromosomePopA, ref Chromosome[] chromosomePopB){
          //chromosomePopB holds new population
            int numElite = (int) (eliteRate * populationSize);
            if (numElite == 0) {
              Console.WriteLine("either population size is too small or elit rate is too small");
            }
            for (int i = 0; i < numElite; i++) {
              //chromosomePopB holding new chromosomes for elite portion
              chromosomePopB[i] = new Chromosome();
              chromosomePopB[i].appointmentList = chromosomePopA[i].appointmentList;
              chromosomePopB[i].fitness = chromosomePopA[i].fitness;
            }
            return numElite-1;
        }
         (List<Teller>, List<String>) CheckTellerWithoutAnyMatchingCust(CustomerList customers, TellerList tellers){
             //check if there is any teller whose sepciality type doesnt match with any customer service type
            List<Teller> tellerWithNoMatchingCustomer=null;  
            List<String> uniqueTypeOfTellerWithNoMatchingCustomer=null;
            Console.WriteLine("Checking tellers without any matching customer type...");
            Boolean found=false;
            foreach(Teller teller in tellers.Teller){
                string specialityType= teller.specialtyType;
                found=false;
                foreach(Customer customer in customers.Customer){
                    if(specialityType.Equals(customer.type)){
                        found=true;
                        break;
                    }
                }
                if(!found){
                    if(tellerWithNoMatchingCustomer==null){
                        tellerWithNoMatchingCustomer= new List<Teller>();
                    } 
                    if(uniqueTypeOfTellerWithNoMatchingCustomer==null){
                        uniqueTypeOfTellerWithNoMatchingCustomer= new List<string>();
                    }
                    if(!uniqueTypeOfTellerWithNoMatchingCustomer.Contains(specialityType)){
                        uniqueTypeOfTellerWithNoMatchingCustomer.Add(specialityType);
                        
                         Console.WriteLine(specialityType);
                    }
                    tellerWithNoMatchingCustomer.Add(teller);
                }
            }
            return (tellerWithNoMatchingCustomer,uniqueTypeOfTellerWithNoMatchingCustomer);
        }
        
        double getRandomNumberBetween(double min, double max){
          return min + random.NextDouble() * (max - min);
        }
  }


}