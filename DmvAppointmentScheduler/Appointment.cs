﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DmvAppointmentScheduler
{
    public class Appointment
    {
        public Customer customer { get; set; }
        public Teller teller { get; set; }
        public double duration { get; set; }
        public Appointment(Customer customer, Teller teller)
        {
            this.customer = customer;
            this.teller = teller;
            if (customer.type == teller.specialtyType)
            {
                this.duration = Math.Ceiling(Convert.ToDouble(customer.duration) * Convert.ToDouble(teller.multiplier));
            }
            else
            {
                this.duration = Convert.ToDouble(customer.duration);
            }
        }

        public void reCalculateDuration()
        {
            if (customer.type == teller.specialtyType)
            {
                this.duration = Math.Ceiling(Convert.ToDouble(customer.duration) * Convert.ToDouble(teller.multiplier));
            }
            else
            {
                this.duration = Convert.ToDouble(customer.duration);
            }
        }

         public override string ToString()
        {
            return String.Format("Customer : id= {0}\ttype= {1}\tserving teller: id= {2}\ttype = {3} \t appointment duration = {4}",customer.Id, customer.type, teller.id, teller.specialtyType, duration);
        }
    }
}
