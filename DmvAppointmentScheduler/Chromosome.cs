using System.Collections.Generic;
namespace DmvAppointmentScheduler
{
    public class Chromosome{
        //contains 1000 appointments for 1000 customers
        public double fitness {get; set;}
        public List<Appointment> appointmentList{get; set;}
    public Chromosome(){
        fitness=0;
        appointmentList=new List<Appointment>();
    }

    }

}