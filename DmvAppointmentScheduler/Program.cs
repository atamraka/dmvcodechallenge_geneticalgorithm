﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DmvAppointmentScheduler
{
    class Program
    {
        public static Random random = new Random();
        //public static List<Appointment> appointmentList = new List<Appointment>();
        public static Chromosome bestAppointmentSetting;
        static void Main(string[] args)
        {
            CustomerList customers = ReadCustomerData();
            TellerList tellers = ReadTellerData();
            Calculation(customers, tellers);
            OutputTotalLengthToConsole();

        }
        private static CustomerList ReadCustomerData()
        {
            string fileName = "CustomerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            CustomerList customerData = JsonConvert.DeserializeObject<CustomerList>(jsonString);
            return customerData;

        }
        private static TellerList ReadTellerData()
        {
            string fileName = "TellerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            TellerList tellerData = JsonConvert.DeserializeObject<TellerList>(jsonString);
            return tellerData;

        }
        static void Calculation(CustomerList customers, TellerList tellers)
        {
            // Your code goes here .....
            // Re-write this method to be more efficient instead of a assigning all customers to the same teller
            // foreach(Customer customer in customers.Customer)
            // {
            //     var appointment = new Appointment(customer, tellers.Teller[0]);
            //     appointmentList.Add(appointment);
            // }
            GeneticAlgorithm geneticAlgorithm =  new GeneticAlgorithm(customers, tellers);
            bestAppointmentSetting= geneticAlgorithm.startGA();
            Console.WriteLine("####################################################################################");
            Console.WriteLine("####################################################################################");
            Console.WriteLine("Fitness of returned best chromosome = {0}", bestAppointmentSetting.fitness);

        }
        static void OutputTotalLengthToConsole()
        {
            var tellerAppointments =
                from appointment in bestAppointmentSetting.appointmentList
                group appointment by appointment.teller into tellerGroup
                select new
                {
                    teller = tellerGroup.Key,
                    totalDuration = tellerGroup.Sum(x => x.duration),
                };
            var max = tellerAppointments.OrderBy(i => i.totalDuration).LastOrDefault();
            Console.WriteLine("Teller " + max.teller.id + " will work for " + max.totalDuration + " minutes!");
            Console.WriteLine("####################################################################################");
            Console.WriteLine("####################################################################################");
        }

    }
}
